import React, {Component} from 'react';
import database from './database.json'
import map from './assets/KMUTT.png';
export default class App extends Component {
  constructor(props) {

    super(props)

    this.state = {
      showHint: true,
      searchType: "from",
      search: "",
      searchFrom: window
        .location
        .search
        .substring(1)
        .replace(new RegExp("%20", 'g'), " "),
      searchTo: "",
      path: "",
      activeFrom: "",
      activeTo: "",
      alertText: ""
    }

    this.handleSearchFromOnChange = this
      .handleSearchFromOnChange
      .bind(this);
    this.handleSearchToOnChange = this
      .handleSearchToOnChange
      .bind(this);

  }

  handleSearchFromOnChange(event) {
    this.setState({search: event.target.value, searchFrom: event.target.value, path: ""});
    this.placeActive(event.target.value, true)
  }

  handleSearchToOnChange(event) {
    this.setState({search: event.target.value, searchTo: event.target.value, path: ""});
    this.placeActive(event.target.value)
  }

  placeActive(name, to) {
    let activeImagePath = ""
    let query = database
      .filter(word => word.query.toLowerCase().includes(name.toLowerCase()))
      .map((data) => {
        return data
          .query
          .toLowerCase()
      })
    if (query[0]) {
      if ((query[0]).includes("classroom building 1")) {
        activeImagePath = require('./assets/places/cb1.png')
      } else if ((query[0]).includes("classroom building 2")) {
        activeImagePath = require('./assets/places/cb2.png')
      } else if ((query[0]).includes('king mongkut memorial building')) {
        activeImagePath = require('./assets/places/KFC.png')
      } else if ((query[0]).includes('school of information technology')) {
        activeImagePath = require('./assets/places/SIT.png')
      }
    }
    if (to) {
      this.setState({activeFrom: activeImagePath})
    } else {
      this.setState({activeTo: activeImagePath})
    }
  }

  searchType(type) {
    this.setState({searchType: type})
  }

  searchSelected(name) {
    let {searchType, searchFrom, searchTo} = this.state
    if (searchType === "from") {
      if (name !== searchFrom) {
        this.setState({searchFrom: name, path: ""})
        this.placeActive(name, true)
      }
      this
        .nameInput
        .focus()
      this.setState({searchType: 'to'})
    } else {
      if (name !== searchTo) {
        this.setState({searchTo: name, path: ""})
        this.placeActive(name)
      }
    }
  }

  route() {
    let {searchFrom, searchTo} = this.state
    if (searchFrom === searchTo) {
      this.setState({alertText: "You are entering an existing input!!! - คุณกรอกข้อมูลที่เคยกรอกแล้ว"})
    } else {
      this.setState({showHint: false})
      if (["Classroom Building 1", "King Mongkut Memorial Building"].includes(searchFrom) && ["Classroom Building 1", "King Mongkut Memorial Building"].includes(searchTo)) {
        this.setState({path: require('./assets/paths/cb1_to_kfc.png')})
      } else if (["Classroom Building 2", "King Mongkut Memorial Building"].includes(searchFrom) && ["Classroom Building 2", "King Mongkut Memorial Building"].includes(searchTo)) {
        this.setState({path: require('./assets/paths/cb2_to_kfc.png')})
      } else if (searchFrom === "School of Information Technology" && searchTo === "Classroom Building 2") {
        this.setState({path: require('./assets/paths/sit_to_cb2.png')})
      } else {
        this.setState({path: "", alertText: ""})
      }
    }
  }

  clearState() {
    this.setState({
      searchType: "from",
      search: "",
      searchFrom: "",
      searchTo: "",
      path: "",
      activeFrom: "",
      activeTo: "",
      alertText: ""
    })
  }

  toggleHint() {
    if (this.state.showHint) {
      this.setState({showHint: false})
    } else {
      this.setState({showHint: true})
    }
  }

  componentDidMount() {
    if (this.state.searchFrom) {
      this.placeActive(this.state.searchFrom, true)
      this
        .nameInput
        .focus()
      this.setState({searchType: 'to'})
    }
  }

  render() {

    let {
      showHint,
      search,
      searchFrom,
      searchTo,
      path,
      activeFrom,
      activeTo,
      alertText
    } = this.state;
    var transparent = require('./assets/KMUTT_transparent.png');

    return (
      <div
        className="container text-light shadow-lg pb-3"
        style={{
        background: 'coral'
      }}>
        <div className="row">
          {alertText
            ? <div
                className={"col-12 alert alert-danger"}
                onClick={() => this.setState({alertText: ""})}>
                {alertText}
              </div>
            : null}
          <a
            href={window.location.origin + window.location.pathname}
            className="col-12 card-link text-dark font-weight-light h1 pt-3 text-truncate"><i className="fas fa-map text-secondary"/> Bangmod Map</a>
          <hr className="col m-0"/>
        </div>
        <div className="row">
          <div className="col-lg-4 col-12 mt-2">
            <input
              className="form-control"
              onSelect={() => this.searchType('from')}
              onChange={this.handleSearchFromOnChange}
              placeholder="From"
              value={searchFrom}/>
          </div>
          <div className="col-lg-4 col-12 mt-2">
            <input
              className="form-control"
              ref={(input) => {
              this.nameInput = input
            }}
              onSelect={() => this.searchType('to')}
              onChange={this.handleSearchToOnChange}
              placeholder="To"
              value={searchTo}/>
          </div>
          <div className="col-lg-4 col-12">
            <div className="row">
              <div className="col-lg-5 col-6 mt-2 pr-1">
                <div className="btn btn-primary btn-block" onClick={() => this.route()}><i className="fas fa-paper-plane"/></div>
              </div>
              <div className="col-lg-5 col-6 mt-2 pl-1">
                <div className="btn btn-danger btn-block" onClick={() => this.clearState()}><i className="fas fa-trash-alt"/></div>
              </div>
              <div className="col-lg-2 col-12 mt-2 pl-lg-0">
                <div className="btn btn-light btn-block" onClick={() => this.toggleHint()}><i
                  className={"fas fa-angle-double-" + (showHint
        ? "up"
        : "down")}/></div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          {showHint
            ? database
              .filter(word => word.query.toLowerCase().includes(search.toLowerCase()))
              .map((data, index) => {
                return (
                  <div className="col-lg-4 col-md-6 mt-2" key={index}>
                    <div
                      className="btn btn-dark btn-block text-truncate"
                      onClick={() => this.searchSelected(data.name)}>{data.name}<br/>{data.nameTH}
                    </div>
                  </div>
                )
              })
            : null}
        </div>
        <div className="row mt-2">
          <div className="col-12">
            <div
              style={{
              background: 'white url(' + map + ')',
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'contain'
            }}>
              <div
                style={searchFrom || searchTo
                ? {
                  background: 'rgba(255,255,255,0.7)'
                }
                : null}>
                <div
                  style={{
                  backgroundImage: 'url(' + activeFrom + ')',
                  backgroundRepeat: 'no-repeat',
                  backgroundSize: 'contain'
                }}>
                  <div
                    style={{
                    backgroundImage: 'url(' + activeTo + ')',
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'contain'
                  }}>
                    <div
                      style={{
                      backgroundImage: 'url(' + path + ')',
                      backgroundRepeat: 'no-repeat',
                      backgroundSize: 'contain'
                    }}>
                      <img
                        src={transparent}
                        alt="map"
                        className="img-responsive"
                        width="100%"
                        height="100%"/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
